<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>S1 User Function</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>004b7443-9ac1-41e5-9074-254dcdbf7654</testSuiteGuid>
   <testCaseLink>
      <guid>8d4f7564-f6c2-4d46-bac7-ca576d7f9047</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Blocks/02 Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>34a5956a-5457-424e-9df0-506afd69baf9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/account_list</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>34a5956a-5457-424e-9df0-506afd69baf9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>1fa25e6d-540d-4664-ae40-3598601dd161</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>34a5956a-5457-424e-9df0-506afd69baf9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>d720767b-1896-49ea-9675-36e30b69695a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
