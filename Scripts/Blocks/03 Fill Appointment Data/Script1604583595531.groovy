import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.selectOptionByValue(findTestObject('Object Repository/Registration Page/select_facility'), 'Hongkong CURA Healthcare Center', 
    false)

WebUI.click(findTestObject('Object Repository/Registration Page/checkbox_applyHospitalReadmission'))

WebUI.click(findTestObject('Object Repository/Registration Page/radioButton_healthcareProgram'))

String date = CustomKeywords.'com.ksat002.function.util.DateUtil.addMonth'()

WebUI.setText(findTestObject('Object Repository/Registration Page/input_VisitDate'), date)

//WebUI.click(findTestObject('Object Repository/Registration Page/select_date25'))

WebUI.setText(findTestObject('Object Repository/Registration Page/input_comment'), comment + ' is the name of the patient')

WebUI.click(findTestObject('Object Repository/Registration Page/button_bookAppointment'))

String facility = WebUI.getText(findTestObject('CURA - Confirmation Page/label_facility'))

WebUI.verifyMatch(facility, 'Hongkong.*', true)

WebUI.verifyElementText(findTestObject('CURA - Confirmation Page/label_applyForHospital'), 'Yes')

WebUI.verifyElementText(findTestObject('CURA - Confirmation Page/label_healthCareProgram'), 'Medicaid')

WebUI.verifyElementText(findTestObject('CURA - Confirmation Page/label_visitDate'), date)

WebUI.verifyElementText(findTestObject('CURA - Confirmation Page/label_comment'), comment + ' is the name of the patient')
